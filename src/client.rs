use crate::mcserver::{
    TMP_DIR,
    InstanceType,
    MCVersion
};
use std::{
    mem,
    os::unix::net::UnixStream,
    fmt::{Display, Formatter, Result as FmtResult},
    net::Shutdown,
    io::{prelude::*, BufReader, Error as IOError},
    slice::Iter
};
use reqwest::Error as WebError;
use ferinth::Error as FerinthError;
use bincode::Error as BincodeError;
use serde_derive::{Deserialize, Serialize};
use lazy_static::lazy_static;

lazy_static! {
    pub static ref SOCKET: String = format!("{}/socket", *TMP_DIR);
}

pub type ServerResult<T> = Result<T, ServerError>;

pub enum ServerError {
    WebError(WebError),
    FerinthError(FerinthError),
    ResponseParseError(BincodeError),
    SocketError(IOError),
    Message(String),
    DownloadErrors(Vec<ServerError>)
}

impl From<WebError> for ServerError {
    fn from(error: WebError) -> Self { Self::WebError(error) }
}

impl From<FerinthError> for ServerError {
    fn from(error: FerinthError) -> Self { Self::FerinthError(error) }
}

impl From<BincodeError> for ServerError {
    fn from(error: BincodeError) -> Self { Self::ResponseParseError(error) }
}

impl From<IOError> for ServerError {
    fn from(error: IOError) -> Self { Self::SocketError(error) }
}

impl From<String> for ServerError {
    fn from(msg: String) -> Self { Self::Message(msg) }
}

impl From<&str> for ServerError {
    fn from(msg: &str) -> Self { Self::Message(msg.to_string()) }
}

impl From<Vec<ServerError>> for ServerError {
    fn from(errors: Vec<ServerError>) -> Self { Self::DownloadErrors(errors) }
}

impl Display for ServerError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Self::WebError(error) => {
                if let Some(url) = error.url() {
                    write!(f, "Unable to complete request to {url}: {error}")
                }
                else {
                    write!(f, "Unable to complete request: {error}")
                }
            }

            Self::FerinthError(error) => write!(f, "A Modrinth API error has occurred: {error}"),
            Self::SocketError(error) => write!(f, "A Unix socket error has occurred: {error}"),
            Self::ResponseParseError(error) => write!(f, "Unable to parse repsonse data: {error}"),
            Self::Message(msg) => write!(f, "{msg}"),
            Self::DownloadErrors(errors) => {
                writeln!(f, "Errors downloading Minecraft server files:")?;
                for error in errors {
                    writeln!(f, "{error}")?;
                }

                Ok(())
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ConsoleLines(Vec<String>);

impl ConsoleLines {
    pub fn empty() -> ConsoleLines { ConsoleLines(Vec::new()) }
    pub fn len(&self) -> usize { self.0.len() }
    pub fn lines(&self) -> Iter<String> { self.0.iter() }
}

impl<S: Display> FromIterator<S> for ConsoleLines {
    fn from_iter<T: IntoIterator<Item = S>>(iter: T) -> Self {
        let lines: Vec<String> = iter.into_iter().map(|v| v.to_string()).collect();
        ConsoleLines(lines)
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum ClientRequest {
    SetupServer {
        name: String,
        java: String,
        mc_version: MCVersion,
        eula: bool,
        instance_type: InstanceType,
        no_chat_reports: bool
    },

    ServerRunning(String),
    Delete(String),
    Launch(String),
    Restart(String),
    Stop(String),
    UpdateModloader(String),
    Console(String, usize),
    ExecCmd(String, String),
    Rename(String, String),
    ImportServers
}

impl ClientRequest {
    pub fn send<T>(self) -> ServerResult<T>
        where T: serde::Serialize + for <'de> serde::Deserialize<'de>
    {
        let encoded = bincode::serialize(&self).unwrap();
        let mut socket = UnixStream::connect(&*SOCKET)?;

        socket.write_all(&encoded).unwrap();
        socket.shutdown(Shutdown::Write).unwrap();
        let buffer = BufReader::new(&socket);
        let encoded: Vec<u8> = buffer.bytes().map(Result::unwrap_or_default).collect();

        if mem::size_of::<T>() == 0 {
            if !encoded.is_empty() {
                return Err(bincode::deserialize::<String>(&encoded)?)?;
            }

            let zst = bincode::deserialize::<T>(&[])?;
            Ok(zst)
        }
        else {
            match bincode::deserialize::<T>(&encoded) {
                Ok(body) => Ok(body),
                Err(_) => {
                    // if the response is an error message, return it
                    Err(bincode::deserialize::<String>(&encoded)?)?
                }
            }
        }
    }

    pub fn send_empty(self) -> ServerResult<()> { self.send() }
}