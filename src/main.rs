use std::{
    env,
    time::Duration
};
use rust_utils::{utils, logging::Log};
use lazy_static::lazy_static;
use reqwest::blocking::{ClientBuilder, Client};
use ferinth::Ferinth;

mod daemon;
mod mcserver;
mod tui;
mod client;
mod cli;
mod config;

lazy_static! {
    static ref LOG: Log = Log::new(utils::get_execname().as_str(), "serverman").main_log(true);
    static ref MODRINTH: Ferinth = Ferinth::default();
    static ref DEBUG: bool = should_debug();
    static ref CLIENT: Client = ClientBuilder::new()
        .timeout(None)
        .connect_timeout(Duration::from_secs(30))
        .build()
        .unwrap();
}

const EULA_URL: &str = "https://www.minecraft.net/en-us/eula";
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    LOG.report_panics(true);
    match utils::get_execname().as_str() {
        "servermancli" => cli::parse_args(),
        "servermand" => daemon::init(),
        "serverman" => tui::init(),
        _ => unreachable!()
    }
}

// are we in debug mode?
fn should_debug() -> bool {
    // get the runtime arguments
    let mut args = env::args();

    // should debug messages be shown?
    if let Some(arg) = args.nth(1) {
        arg.as_str() == "-d"
    }
    else {
        false
    }
}