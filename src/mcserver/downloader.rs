use super::{MCVersion, InstanceType, ServerProfile};
use crate::{
    LOG, CLIENT,
    client::ServerResult
};
use std::{
    env, fs,
    thread::{self, JoinHandle}
};
use regex::Regex;
use lazy_static::lazy_static;
use rust_utils::logging::LogLevel;

lazy_static! {
    static ref MC_RE1: Regex = Regex::new(r"https://launcher.mojang.com/v1/objects/[a-z0-9]*/server.jar").expect("Invalid regex!");
    static ref MC_RE2: Regex = Regex::new(r"https://piston-data.mojang.com/v1/objects/[a-z0-9]*/server.jar").expect("Invalid regex!");
    pub static ref MCVERSIONS: String = format!("{}/.local/share/serverman/versions", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref FORGE_JARS: String = format!("{}/.local/share/serverman/forge_jars", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref FABRIC_JARS: String = format!("{}/.local/share/serverman/fabric_jars", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref NCR_JARS: String = format!("{}/.local/share/serverman/ncr_jars", env::var("HOME").expect("Where the hell is your home folder?"));
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum DownloadConfig {
    ServerSetup,
    UpdateForge,
    UpdateFabric {
        loader: bool,
        api: bool
    }
}

type DownloadThread = JoinHandle<ServerResult<()>>;

pub struct ServerDownloader<'p> {
    profile: &'p ServerProfile,
    no_chat_reports: bool,
    config: DownloadConfig,
    download_threads: Vec<DownloadThread>
}

impl<'p> ServerDownloader<'p> {
    pub fn server_setup(profile: &'p ServerProfile, no_chat_reports: bool) -> Self {
        Self::new(profile, DownloadConfig::ServerSetup, no_chat_reports)
    }

    pub fn update_forge(profile: &'p ServerProfile) -> Self {
        Self::new(profile, DownloadConfig::UpdateForge, false)
    }

    pub fn update_fabric(profile: &'p ServerProfile, loader: bool, api: bool) -> Self {
        Self::new(profile, DownloadConfig::UpdateFabric { loader, api }, false)
    }

    #[inline]
    fn new(profile: &'p ServerProfile, config: DownloadConfig, no_chat_reports: bool) -> Self {
        ServerDownloader {
            profile,
            no_chat_reports,
            config,
            download_threads: Vec::new()
        }
    }

    pub fn start_download(&mut self) {
        fs::create_dir_all(&*FORGE_JARS).unwrap();
        fs::create_dir_all(&*FABRIC_JARS).unwrap();
        env::set_current_dir(&self.profile.directory).expect("Does this directory even exist?");
        if self.profile.instance_type != InstanceType::Vanilla {
            fs::create_dir("mods").unwrap_or(());
        }

        // download the minecraft server JAR file
        if self.config == DownloadConfig::ServerSetup {
            self.add_downloader(|mc_version| {
                // if the file already exists, do not redownload it
                let server_file = format!("minecraft_server.{mc_version}.jar");
                if let Ok(mcs_file) = fs::read(format!("{}/{server_file}", *MCVERSIONS)) {
                    fs::write(&server_file, mcs_file).unwrap();
                    write_l4jconfig(mc_version);
                    return Ok(());
                }

                // get the download link for the specific Minecraft version
                let moj_url = if mc_version == MCVersion::V20W14INF {
                    // 20w14infinite download link
                    "https://launcher.mojang.com/v1/objects/c0711cd9608d1af3d6f05ac423dd8f4199780225/server.jar".to_string()
                }
                else {
                    let body = CLIENT.get(format!("https://mcversions.net/download/{mc_version}"))
                        .send()?
                        .text()?;

                    if let Some(url) = MC_RE1.find(&body) {
                        url.as_str().to_string()
                    }
                    else {
                        MC_RE2.find(&body)
                            .ok_or("Where is the server downloader URL?")?
                            .as_str()
                            .to_string()
                    }
                };

                LOG.line_basic(format!("Downloading {server_file}"), true);
                let data = CLIENT.get(moj_url)
                    .send()?
                    .bytes()?;

                let version_path = format!("{}/minecraft_server.{mc_version}.jar", *MCVERSIONS);
                fs::create_dir_all(&*MCVERSIONS).unwrap_or(());
                fs::write(version_path, data).unwrap_or(());
                fs::copy(format!("{}/{server_file}", *MCVERSIONS), &server_file).expect("Server file does not exist!");
                LOG.line_basic("Minecraft server download is complete!", true);
                write_l4jconfig(mc_version);
                Ok(())
            });
        }

        if let Ok(modloader_ver) = self.profile.instance_type.latest_version(self.profile.mc_version) {
            // download the installer for the specified modloader
            let dl_config = self.config;
            match self.profile.instance_type {
                InstanceType::Fabric => {
                    let (loader_ver, api_ver) = modloader_ver.fabric_versions().unwrap();
                    let (loader, api) = if let DownloadConfig::UpdateFabric { loader, api } = dl_config {
                        (loader, api)
                    }
                    else { (false, false) };

                    // download the fabric loader
                    self.add_downloader(move |mc_version| {
                        let fabric_name = format!("fabric-server-mc.{mc_version}-loader.{loader_ver}");
                        if let Ok(dir) = fs::read_dir(&*FABRIC_JARS) {
                            for file in dir.filter_map(Result::ok) {
                                let name = file.file_name().into_string().unwrap();
                                let path = file.path();
                                if name.starts_with(&fabric_name) {
                                    // if the loader has already been downloaded just copy the file into the server directory
                                    fs::copy(path, &name).unwrap();
                                    return Ok(());
                                }
                            }
                        }

                        if loader || dl_config == DownloadConfig::ServerSetup {
                            // get the latest installer version
                            let body = CLIENT.get("https://meta.fabricmc.net/v2/versions/installer")
                                .send()?
                                .text()?;

                            let json = json::parse(&body).unwrap();
                            let installer_ver = json[0]["version"].as_str().unwrap().to_string();
                            let fabric_jar = format!("{fabric_name}-launcher.{installer_ver}.jar");
                            let dl_url = format!("https://meta.fabricmc.net/v2/versions/loader/{mc_version}/{loader_ver}/{installer_ver}/server/jar");

                            LOG.line_basic(format!("Downloading {fabric_jar}"), true);
                            let data = CLIENT.get(dl_url)
                                .send()?
                                .bytes()?;

                            fs::write(format!("{}/{fabric_jar}", *FABRIC_JARS), &data).unwrap();
                            fs::write(&fabric_jar, data).expect("Fabric loader file does not exist!");
                            LOG.line_basic("Fabric loader download is complete!", true);
                        }

                        Ok(())
                    });

                    // download the fabric api mod
                    self.add_downloader(move |mc_version| {
                        let api_jar_name = format!("fabric-api-{api_ver}+{mc_version}.jar");

                        if let Ok(dir) = fs::read_dir(&*FABRIC_JARS) {
                            for file in dir.filter_map(Result::ok) {
                                let name = file.file_name().into_string().unwrap();
                                let path = file.path();

                                if name == api_jar_name {
                                    // if the api mod has already been downloaded just copy the file into the server directory
                                    fs::copy(path, format!("mods/{name}")).unwrap();
                                    return Ok(());
                                }
                            }
                        }

                        if api || dl_config == DownloadConfig::ServerSetup {
                            let url = mc_version.fabric_api_url()?;

                            LOG.line_basic(format!("Downloading {api_jar_name}"), true);
                            let api_data = CLIENT.get(url)
                                .send()?
                                .bytes()?;

                            fs::write(format!("{}/{api_jar_name}", *FABRIC_JARS), &api_data).unwrap();
                            fs::write(format!("mods/{api_jar_name}"), api_data).expect("Fabric API mod file does not exist!");
                            LOG.line_basic("Fabric API download is complete!", true);
                        }

                        Ok(())
                    });
                }

                InstanceType::Forge => {
                    let forge_ver = if dl_config == DownloadConfig::ServerSetup {
                        self.profile.instance_ver.forge_version().unwrap().to_string()
                    }
                    else if let Ok(version) = self.profile.instance_type.latest_version(self.profile.mc_version) {
                        version.forge_version().unwrap().to_string()
                    }
                    else {
                        LOG.line(LogLevel::Error, "Unable to get latest forge version!", true);
                        return;
                    };

                    // download the forge installer file
                    self.add_downloader(move |mc_version| {
                        let jar_name = format!("forge-{mc_version}-{forge_ver}-installer.jar");
                        let download_url = format!("https://maven.minecraftforge.net/net/minecraftforge/forge/{mc_version}-{forge_ver}/{jar_name}");

                        // if the installer has already been downloaded just copy the file into the server directory
                        if let Ok(dir) = fs::read_dir(&*FORGE_JARS) {
                            for file in dir.filter_map(Result::ok) {
                                let name = file.file_name().into_string().unwrap();
                                let path = file.path();

                                if name == jar_name {
                                    fs::copy(path, &jar_name).unwrap();
                                    return Ok(());
                                }
                            }
                        }

                        if dl_config == DownloadConfig::ServerSetup || dl_config == DownloadConfig::UpdateForge {
                            LOG.line_basic(format!("Downloading {jar_name}"), true);
                            let data = CLIENT.get(download_url)
                                .send()?
                                .bytes()?;

                            fs::write(format!("{}/{jar_name}", *FORGE_JARS), &data).unwrap();
                            fs::write(&jar_name, data).expect("Forge installer file does not exist!");
                            LOG.line_basic("Forge installer download is complete!", true);
                        }

                        Ok(())
                    });
                }

                InstanceType::Vanilla => { }
            }
        }

        // does the user want to install No Chat Reports?
        if self.profile.mc_version >= MCVersion(1, 19, 2) && self.no_chat_reports && self.profile.instance_type != InstanceType::Vanilla {
            let instance_type = self.profile.instance_type;
            self.add_downloader(move |mc_version| {
                fs::create_dir_all(&*NCR_JARS).unwrap();
                let loader = if instance_type == InstanceType::Forge { "FORGE" }
                else { "FABRIC" };
                let ncr_name = format!("NoChatReports-{loader}-{mc_version}");

                if let Ok(dir) = fs::read_dir(&*NCR_JARS) {
                    for file in dir.filter_map(Result::ok) {
                        let name = file.file_name().into_string().unwrap();
                        let path = file.path();

                        if name.starts_with(&ncr_name) {
                            fs::copy(path, format!("mods/{name}")).unwrap();
                            return Ok(());
                        }
                    }
                }

                LOG.line_basic("Downloading No Chat Reports...", true);
                let (url, file_name) = mc_version.no_chat_reports_url(instance_type)?;
                let mod_file = CLIENT.get(url)
                    .send()?
                    .bytes()?;

                LOG.line_basic("Installing No Chat Reports...", true);
                fs::write(format!("{}/{file_name}", *NCR_JARS), &mod_file).unwrap();
                fs::write(format!("mods/{file_name}"), mod_file).unwrap();
                LOG.line_basic("No Chat Reports install complete", true);
                Ok(())
            })
        }
    }

    pub fn finish_download(&mut self) -> ServerResult<()> {
        let mut errors = Vec::new();

        while let Some(thread) = self.download_threads.pop() {
            let result = thread.join().unwrap();
            if let Err(why) = result {
                errors.push(why);
            }
        }

        if errors.is_empty() {
            LOG.line_basic("Server download complete!", true);
            Ok(())
        }
        else {
            Err(errors)?
        }
    }

    fn add_downloader<F: FnOnce(MCVersion) -> ServerResult<()> + Send + 'static>(&mut self, thread: F) {
        let mc_version = self.profile.mc_version;
        self.download_threads.push(thread::spawn(move || thread(mc_version)));
    }
}

// write the config for log4j to the server directory if this is a Minecraft
// version affected by the log4shell exploit
fn write_l4jconfig(version: MCVersion) {
    if version >= MCVersion(1, 12, 0) && version <= MCVersion(1, 16, 0) {
        fs::write("log4j2_112-116.xml", include_str!("log4jcfg.txt")).unwrap();
    }
}