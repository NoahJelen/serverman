use crate::{
    CLIENT, MODRINTH,
    client::ServerResult
};
use std::{
    fmt::{
        Display,
        Formatter,
        Result as FmtResult,
    },
    sync::RwLock,
    time::Instant,
    collections::HashMap
};
use lazy_static::lazy_static;
use libium::{
    config::structs::ModLoader,
    upgrade::mod_downloadable
};
use rust_utils::futures::exec_future;
use regex::Regex;
use serde_derive::{Deserialize, Serialize};

lazy_static! {
    static ref FORGE_VER_RE: Regex = Regex::new(r"1\.\d+\.?\d* - \d+\.\d+\.\d+\.?\d*").unwrap();
    static ref FORGE_VERS: RwLock<HashMap<MCVersion, InstanceVersion>> = RwLock::new(HashMap::new());
    static ref FABRIC_VERS: RwLock<HashMap<MCVersion, InstanceVersion>> = RwLock::new(HashMap::new());
    static ref CACHE_TIME: RwLock<Instant> = RwLock::new(Instant::now());
}

#[derive(Deserialize, Serialize, Clone, Hash)]
pub enum InstanceVersion {
    Vanilla,
    Fabric {
        loader: (u8, u8, u8),
        api: (u8, u8, u8)
    },
    Forge(String)
}

impl InstanceVersion {
    pub fn fabric_versions(&self) -> Option<(String, String)> {
        if let Self::Fabric { loader, api } = self {
            let loader_str = {
                let (major, minor, rev) = loader;
                format!("{major}.{minor}.{rev}")
            };

            let api_str = {
                let (major, minor, rev) = api;
                format!("{major}.{minor}.{rev}")
            };

            Some((loader_str, api_str))
        }
        else { None }
    }

    pub fn forge_version(&self) -> Option<&str> {
        if let Self::Forge(ref forge_ver) = self {
            Some(forge_ver)
        }
        else {
            None
        }
    }
}

#[derive(Deserialize, Serialize, Copy, Clone, PartialEq, Eq)]
pub enum InstanceType {
    Vanilla,
    Fabric,
    Forge
}

impl InstanceType {
    fn get_modloader(self) -> Option<ModLoader> {
        match self {
            Self::Forge => Some(ModLoader::Forge),
            Self::Fabric => Some(ModLoader::Fabric),
            Self::Vanilla => None
        }
    }

    pub fn latest_version(self, mc_version: MCVersion) -> ServerResult<InstanceVersion> {
        if let Some(version) = self.get_cached_version(mc_version) {
            return Ok(version);
        }

        let new_version = match self {
            Self::Forge => {
                // I hate scraping websites!
                let body = CLIENT.get(format!("https://files.minecraftforge.net/net/minecraftforge/forge/index_{mc_version}.html"))
                    .send()?
                    .text()?;

                let version_raw = FORGE_VER_RE.find(&body)
                    .ok_or("Where is the forge version?")?
                    .as_str()
                    .to_string();

                let version = version_raw.split(" - ").nth(1).unwrap().to_string();
                InstanceVersion::Forge(version)
            }

            Self::Fabric => {
                // get the latest loader version
                let body = CLIENT.get(format!("https://meta.fabricmc.net/v1/versions/loader/{mc_version}"))
                    .send()?
                    .text()?;

                let body_json = json::parse(&body).expect("I thought this is a JSON response!");
                let version_str = &body_json[0]["loader"]["version"].as_str().unwrap();
                let loader = parse_ver(version_str);

                // get the API version
                let api_versions = exec_future(MODRINTH.list_versions("P7dR8mSH"))?;

                let mcv_str = mc_version.to_string();
                let api_ver_str = mod_downloadable::get_latest_compatible_version(
                    &api_versions,
                    Some(&mcv_str),
                    Some(&ModLoader::Fabric)
                )
                    .ok_or("Unable to get Fabric API versions!")?
                    .1
                    .version_number
                    .split('+')
                    .next()
                    .ok_or("Unable to get Fabric API versions!")?
                    .to_string();

                let api = parse_ver(&api_ver_str);
                InstanceVersion::Fabric {
                    loader,
                    api
                }
            }

            Self::Vanilla => return Ok(InstanceVersion::Vanilla)
        };

        self.cache_ver(mc_version, new_version.clone());
        Ok(new_version)
    }

    fn cache_ver(self, mc_version: MCVersion, version: InstanceVersion) {
        let mut versions = match self {
            Self::Fabric => FABRIC_VERS.write().unwrap(),
            Self::Forge => FORGE_VERS.write().unwrap(),
            Self::Vanilla => return
        };

        versions.insert(mc_version, version);
    }

    fn get_cached_version(self, mc_version: MCVersion) -> Option<InstanceVersion> {
        let mut timer = CACHE_TIME.write().unwrap();
        if timer.elapsed().as_secs() / 86400 > 1 {
            let mut versions = match self {
                Self::Fabric => FABRIC_VERS.write().unwrap(),
                Self::Forge => FORGE_VERS.write().unwrap(),
                Self::Vanilla => return None
            };

            versions.clear();
            *timer = Instant::now();
            None
        }
        else {
            let versions = match self {
                Self::Fabric => FABRIC_VERS.read().unwrap(),
                Self::Forge => FORGE_VERS.read().unwrap(),
                Self::Vanilla => return None
            };

            Some(versions.get(&mc_version)?.clone())
        }
    }
}

// vanilla Minecraft version
#[derive(Copy, Clone, Eq, PartialEq, PartialOrd, Deserialize, Serialize, Hash)]
pub struct MCVersion(pub u8, pub u8, pub u8);

impl MCVersion {
    pub const V20W14INF: Self = MCVersion(255, 255, 255);

    pub fn fabric_api_url(self) -> ServerResult<String> {
        Ok(self.get_mod_url("P7dR8mSH", InstanceType::Fabric)?.0)
    }

    pub fn no_chat_reports_url(self, instance_type: InstanceType) -> ServerResult<(String, String)> {
        self.get_mod_url("qQyHxfxd", instance_type)
    }

    fn get_mod_url(self, proj_id: &str, instance_type: InstanceType) -> ServerResult<(String, String)> {
        let mcv_str = self.to_string();
        let mod_versions = exec_future(MODRINTH.list_versions(proj_id))?;

        let mod_file = mod_downloadable::get_latest_compatible_version(
            &mod_versions,
            Some(&mcv_str),
            Some(
                &instance_type.get_modloader()
                    .ok_or("Vanilla doesn't have a modloader!")?
            )
        )
            .ok_or("Unable to get mod file!")?
            .0;

        Ok((mod_file.url.to_string(), mod_file.filename))
    }
}

// parse the minecraft version from string input
impl From<&str> for MCVersion {
    fn from(val: &str) -> MCVersion {
        if val == "20w14infinite" { return Self::V20W14INF; }
        let (major, minor, rev) = parse_ver(val);
        MCVersion(major, minor, rev)
    }
}

impl Display for MCVersion {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let Self(major, minor, rev) = *self;
        if *self == Self::V20W14INF {
            write!(f, "20w14infinite")
        }
        else if rev == 0 {
            write!(f, "{major}.{minor}")
        }
        else {
            write!(f, "{major}.{minor}.{rev}")
        }
    }
}

#[derive(Deserialize, Serialize, Clone)]
pub struct JavaOptions {
    // command used to launch java
    pub launch_command: String,

    // minimum memory in megabytes
    pub min_mem: usize,

    // maximum memory in megabytes
    pub max_mem: usize
}

impl Default for JavaOptions {
    fn default() -> JavaOptions {
        JavaOptions {
            launch_command: String::from("java"),
            min_mem: 1024,
            max_mem: 1024
        }
    }
}

#[derive(Copy, Clone)]
pub enum GameMode {
    Survival,
    Creative,
    Adventure,
    Spectator
}

impl From<&str> for GameMode {
    fn from(val: &str) -> Self {
        match val.to_lowercase().as_str() {
            "creative" | "1" => GameMode::Creative,
            "adventure" | "2" => GameMode::Adventure,
            "spectator" | "3" => GameMode::Spectator,
            _ => GameMode::Survival
        }
    }
}

impl Display for GameMode {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            GameMode::Survival => write!(f, "survival"),
            GameMode::Creative => write!(f, "creative"),
            GameMode::Adventure => write!(f, "adventure"),
            GameMode::Spectator => write!(f, "spectator")
        }
    }
}

#[derive(Copy, Clone)]
pub enum Difficulty {
    Peaceful,
    Easy,
    Normal,
    Hard
}

impl From<&str> for Difficulty {
    fn from(string: &str) -> Difficulty {
        match string.to_lowercase().as_str() {
            "easy" | "1" => Difficulty::Easy,
            "normal" | "2" => Difficulty::Normal,
            "hard" | "3" => Difficulty::Hard,
            _ => Difficulty::Peaceful
        }
    }
}

impl Display for Difficulty {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            Difficulty::Peaceful => write!(f, "peaceful"),
            Difficulty::Easy => write!(f, "easy"),
            Difficulty::Normal => write!(f, "normal"),
            Difficulty::Hard => write!(f, "hard")
        }
    }
}

fn parse_ver(version_str: &str) -> (u8, u8, u8) {
    let nums: Vec<u8> = version_str.split('.')
        .map(str::parse)
        .filter_map(Result::ok)
        .collect();

    let rev = nums.get(2)
        .copied()
        .unwrap_or(0);

    (nums[0], nums[1], rev)
}