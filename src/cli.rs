use crate::{
    VERSION, EULA_URL,
    client::ClientRequest,
    config::{GlobalConfig, Config},
    mcserver::{MCVersion, InstanceType},
    tui::console
};
use std::env;
use colored::Colorize;
use cursive_extras::buffered_backend_root;
use rust_utils::utils::get_input;

pub fn parse_args() {
    let mut args = env::args();

    if let Some(cmd) = args.nth(1) {
        match cmd.as_str() {
            "launch" => {
                if let Some(name) = args.next() {
                    let result = ClientRequest::Launch(name.to_string()).send_empty();
                    match result {
                        Ok(()) => println!("Successfully launched \"{name}\" Minecraft server"),
                        Err(error) => println!("Error launching \"{name}\" Minecraft server!\n{error}")
                    }
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "restart" | "stop" => {
                if let Some(name) = args.next() {
                    let restart = cmd.as_str() == "restart";

                    let result = if restart {
                        ClientRequest::Restart(name.to_string()).send_empty()
                    }
                    else {
                        ClientRequest::Stop(name.to_string()).send_empty()
                    };

                    match result {
                        Ok(()) if restart => println!("Successfully restarted \"{name}\" Minecraft server"),
                        Ok(()) => println!("Successfully shut down \"{name}\" Minecraft server"),
                        Err(why) => eprintln!("Unable to {cmd} \"{name}\" Minecraft server: {why}")
                    }
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "console" => {
                if let Some(name) = args.next() {
                    let mut root = buffered_backend_root();
                    console::show(&mut root, &name, true);
                    root.run();
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "remove" => {
                if let Some(name) = args.next() {
                    let result = ClientRequest::Delete(name.to_string()).send_empty();
                    match result {
                        Ok(()) => println!("Removed \"{name}\" Minecraft server"),
                        Err(error) => eprintln!("Error removing \"{name}\" Minecraft server: {error}")
                    }
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "rename" => {
                if let Some(name) = args.next() {
                    if let Some(new_name) = args.next() {
                        println!("Renaming \"{name}\" Minecraft server...");
                        match ClientRequest::Rename(name.to_string(), new_name.to_string()).send_empty() {
                            Ok(()) => println!("\"{name}\" is now known as \"{new_name}\""),
                            Err(error) => eprintln!("Unable to rename \"{name}\" Minecraft server! {error}")
                        }
                    }
                    else { eprintln!("Please specify a new server name!"); }
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "update-modloader" => {
                if let Some(name) = args.next() {
                    update_modloader(&name)
                }
                else { eprintln!("Please specify a server name!"); }
            }

            "servers" => {
                let config = GlobalConfig::load();
                println!("{}\n", "Known Minecraft servers:".bright_white());

                for server in config.profiles {
                    println!("{server}");
                }
            }

            "version" | "-v" => {
                println!("Minecraft Server Manager");
                println!("Version {VERSION}");
            }

            "help" | "-h" => {
                println!("{}","Minecraft Server Manager Help:".bright_white());
                println!("{}", "Subcommands:".bright_white());
                println!("    {} set up a new Minecraft server with optional modloader installation (valid values are forge and fabric)", "setup [modloader]:".bright_white());
                println!("    {} Launch a specified server", "launch <server name>:".bright_white());
                println!("    {} Restart a specified server", "restart <name>:".bright_white());
                println!("    {} Show the console of a specified server", "console <name>:".bright_white());
                println!("    {} Deletes a specified server. Please use this carefully because it cannot be undone!", "remove <name>:".bright_white());
                println!("    {} Renames a specified server.", "rename <name> <new name>:".bright_white());
                println!("    {} Get or set the configuration (server.properties) of a specified server", "config <name> [option] [key] [new value]:".bright_white());
                println!("    {} List all known Minecraft servers", "servers:".bright_white());
                println!("    {} Import all servers not created by this program", "import:".bright_white());
                println!("    {} show this message again", "help:".bright_white());
            }

            "import" => {
                println!("Importing Minecraft servers...");
                if let Err(why) = ClientRequest::ImportServers.send_empty() {
                    eprintln!("{why}");
                }
            }

            "setup" => {
                let modloader = args.next().unwrap_or_default();
                let instance_type = match modloader.as_str() {
                    "forge" => InstanceType::Forge,
                    "fabric" => InstanceType::Fabric,
                    "" => InstanceType::Vanilla,
                    _ => {
                        eprintln!("{modloader} is not a valid modloader!");
                        eprintln!("Valid modloaders are forge and fabric");
                        return;
                    }
                };

                setup_server(instance_type);
            }

            "config" => get_server_cfg(args.next(), args.next(), args.next(), args.next()),
            _ => println!("Invalid option!")
        }
    }
    else {
        eprintln!("Please specify an option!");
    }
}

fn get_server_cfg(name: Option<String>, subcmd: Option<String>, key: Option<String>, new_val: Option<String>) {
    if name.is_none() {
        eprintln!("Please specify a server name!");
        return;
    }
    let name = name.unwrap();

    if name.as_str() == "keys" {
        println!(
            "{}\n\
            allow-flight\n\
            allow-nether\n\
            broadcast-console-to-ops\n\
            broadcast-rcon-to-ops\n\
            difficulty\n\
            enable-command-block\n\
            enable-jmx-monitoring\n\
            enable-query\n\
            enable-rcon\n\
            enable-status\n\
            enforce-whitelist\n\
            entity-broadcast-range-percentage\n\
            force-gamemode\n\
            function-permission-level\n\
            gamemode\n\
            generate-structures\n\
            generator-settings\n\
            hardcore\n\
            level-name\n\
            level-seed\n\
            level-type\n\
            max-build-height\n\
            max-players\n\
            max-tick-time\n\
            max-world-size\n\
            motd\n\
            network-compression-threshold\n\
            online-mode\n\
            op-permission-level\n\
            player-idle-timeout\n\
            prevent-proxy-connections\n\
            pvp\n\
            query.port\n\
            rate-limit\n\
            rcon.password\n\
            rcon.port\n\
            resource-pack\n\
            resource-pack-sha1\n\
            server-ip\n\
            server-port\n\
            snooper-enabled\n\
            spawn-animals\n\
            spawn-monsters\n\
            spawn-npcs\n\
            spawn-protection\n\
            sync-chunk-writes\n\
            text-filtering-config\n\
            use-native-transport\n\
            view-distance\n\
            white-list",
            "List of all known server configuration (server.properties) keys:".bright_white()
        );
        return;
    }
    else if name == "help" {
        println!("{}", "Minecraft server configuration help:".bright_white());
        println!("{}", "Subcommands:".bright_white());
        println!("    {} sets a config key to a new value", "<name> set <key name> <new value>:".bright_white());
        println!("    {} get a config key value", "<name> get <key name>:".bright_white());
        println!("    {} list all known config keys", "keys:".bright_white());
        println!("    {} show this message again", "help:".bright_white());
        return;
    }

    let config = GlobalConfig::load();

    let profile = match config.get_profile(&name) {
        Some(server) => server,
        None => {
            eprintln!("Error retrieving the configuration of \"{name}\" Minecraft server: Server does not exist!");
            return;
        }
    };

    env::set_current_dir(&profile.directory).expect("Where is the directory for the current server?");
    let properties = profile.get_properties();
    if let Some(s) = subcmd {
        if key.is_none() {
            println!("Please specify a config key!");
            return;
        }
        let key = key.unwrap();

        if s.as_str() == "get" {
            if let Some(disp) = properties.get_disp_value(&key) {
                println!("{disp}");
            }
        }
        else if s.as_str() == "set" {
            if new_val.is_none() {
                eprintln!("Please specify a new value for the config key!")
            }

            ClientRequest::ExecCmd(name.to_string(), "reload".to_string())
                .send_empty()
                .unwrap_or(());
        }
        else if s.as_str() == "help" { }
        else { eprintln!("Invalid option!"); }
    }
    else {
        println!("{}{}{}", "Configuration for \"".bright_white(), name.bright_white(), "\" Minecraft server:".bright_white());
        println!("{properties}");
    }
}

fn update_modloader(name: &str) {
    let config = GlobalConfig::load();
    let profile = match config.get_profile(name) {
        Some(server) => server,
        None => {
            eprintln!("Error updating \"{name}\" Minecraft server: Server does not exist!");
            return;
        }
    };

    let modloader = match profile.instance_type {
        InstanceType::Fabric => "Fabric",
        InstanceType::Forge => "Forge",
        InstanceType::Vanilla => {
            eprintln!("\"{name}\" is a vanilla server!");
            return;
        }
    };

    println!("Updating {modloader} for \"{name}\" Minecraft server. This may take a little while...");
    let result = ClientRequest::UpdateModloader(name.to_string()).send_empty();
    if let Err(error) = result {
        eprintln!("Error updating {modloader} for \"{name}\" Minecraft server:\n{error}");
    }
    else {
        println!("Successfully updated {modloader} for \"{name}\" Minecraft server");
    }
}

fn setup_server(instance_type: InstanceType) {
    if instance_type == InstanceType::Forge { println!("Forge will be installed on this server"); }
    else if instance_type == InstanceType::Fabric { println!("Fabric will be installed on this server"); }
    let eula_res = get_input(format!("Do you agree to Mojang's EULA? {EULA_URL} (Please read it) (Y/n)"));
    let java = get_input("Java launch command:");
    let name = get_input("Name:");
    let version = get_input("Version:");
    let mc_version = MCVersion::from(version.as_str());
    let no_chat_reports = if mc_version >= MCVersion(1, 19, 2) && instance_type != InstanceType::Vanilla {
        let ncr_res = get_input("Install No Chat reports? (Y/n)");
        ncr_res.to_lowercase().as_str() == "y" || ncr_res.is_empty()
    }
    else { false };

    println!("Setting up your Minecraft server. This may take a little while...");
    let result = ClientRequest::SetupServer {
        name: name.to_string(),
        java: java.to_string(),
        mc_version,
        eula: eula_res.to_lowercase().as_str() == "y" || eula_res.is_empty(),
        instance_type,
        no_chat_reports
    }
        .send_empty();

    match result {
        Ok(()) => println!("Successfully set up \"{name}\" Minecraft server"),
        Err(why) => eprintln!("Error setting up \"{name}\" Minecraft server: {why}")
    }
}