use crate::{
    LOG,
    config::{GlobalConfig, Config},
    mcserver::{MCVersion, InstanceType, InstanceVersion}
};
use std::env;
use glob::glob;
use walkdir::WalkDir;

pub fn import_servers() {
    LOG.line_basic("Importing Minecraft servers...", true);
    let mut num_servers = 0;
    let mut config = GlobalConfig::load();
    let share_dir = format!("{}/.local/share/serverman", env::var("HOME").unwrap());
    let cfg_dir = format!("{}/.config/serverman", env::var("HOME").unwrap());

    // traverse through every single directory in the default directory, up to a depth of 8
    for dir in WalkDir::new(&config.default_dir).max_depth(8).into_iter().filter_map(Result::ok) {
        let cur_path = dir.path().to_str().unwrap();
        if cur_path.contains(&share_dir) || cur_path.contains(&cfg_dir) { continue; }
        if env::set_current_dir(cur_path).is_ok() {
            // minecraft server names are formatted 2 ways
            let mc_glob = glob("minecraft_server.*.jar").unwrap().next();
            let mc_glob2 = glob("minecraft-*-server.jar").unwrap().next();
            if mc_glob.is_none() && mc_glob2.is_none() { continue; }

            // location of a legacy forge JAR file
            let old_forge_path = glob("forge-*.jar").unwrap().next();

            // location of the forge launch arguments
            let forge_path = glob("libraries/net/minecraftforge/forge/*").unwrap().next();

            // location of the fabric loader
            let fabric_path = glob("fabric-server-mc.*.jar").unwrap().next();

            // location of the fabric API
            let fabric_api_path = glob("mods/fabric-api-*.jar").unwrap().next();

            // default name for the Minecraft server (folder name)
            let name = dir.file_name().to_str().unwrap();

            // what type of Minecraft server is this?
            let instance_type =
                // is this a forge server?
                if forge_path.is_some() || old_forge_path.is_some() { InstanceType::Forge }

                // is this a fabric server?
                else if fabric_path.is_some() && fabric_api_path.is_some() { InstanceType::Fabric }

                else { InstanceType::Vanilla };

            // figure out the modloader version of this server
            let instance_ver = match instance_type {
                InstanceType::Forge => {
                    if let Some(Ok(ver_path)) = forge_path {
                        let name = ver_path.file_name().unwrap().to_str().unwrap();
                        let forge_ver = name.split('-').nth(1).unwrap().to_string();
                        InstanceVersion::Forge(forge_ver)
                    }
                    else {
                       let old_forge = old_forge_path.unwrap().unwrap();
                        let name = old_forge.file_name().unwrap().to_str().unwrap();
                        let name_short = &name[0..name.len() - 4];
                        let forge_ver = name_short.split('-').nth(2).unwrap().to_string();
                        InstanceVersion::Forge(forge_ver)
                    }
                }

                InstanceType::Fabric => {
                    let fabric = fabric_path.unwrap().unwrap();
                    let name = fabric.file_name().unwrap().to_str().unwrap();
                    let mut name_split = name.split('-');
                    let loader_raw = name_split.nth(3).unwrap();
                    let mut loader_split = loader_raw.split('.');
                    loader_split.next();
                    let loader_ver_list: Vec<u8> = loader_split.map(|s| s.parse().unwrap()).collect();
                    let loader = (loader_ver_list[0], loader_ver_list[1], loader_ver_list[2]);
                    let fabric_api = fabric_api_path.unwrap().unwrap();
                    let name = fabric_api.file_name().unwrap().to_str().unwrap();
                    let mut name_split = name.split('-');
                    let mut name_split2 = name_split.nth(2).unwrap().split('+');
                    let ver_str = name_split2.next().unwrap();
                    let api_split = ver_str.split('.');
                    let api_ver_list: Vec<u8> = api_split.map(|s| s.parse().unwrap()).collect();
                    let api = (api_ver_list[0], api_ver_list[1], api_ver_list[2]);

                    InstanceVersion::Fabric{
                        loader,
                        api
                    }
                }

                InstanceType::Vanilla => InstanceVersion::Vanilla
            };

            // figure out the Minecraft version of this server
            if let Some(Ok(server_path)) = mc_glob {
                let server_name = server_path.file_name().unwrap().to_str().unwrap();
                let version = if server_name.contains("20w14infinite") {
                    MCVersion::V20W14INF
                }
                else {
                    let mut name_split = server_name.split('.');
                    let major = name_split.nth(1).unwrap().parse::<u8>().unwrap();
                    let minor = name_split.next().unwrap().parse::<u8>().unwrap();
                    let rev = name_split.next().unwrap_or("0").parse::<u8>().unwrap();
                    MCVersion(major, minor, rev)
                };

                if config.import_server(name, cur_path, version, instance_type, instance_ver) {
                    num_servers += 1;
                }
            }
            else if let Some(Ok(server_path)) = mc_glob2 {
                let server_name = server_path.file_name().unwrap().to_str().unwrap();
                let mut name_split = server_name.split('-');
                let version = MCVersion::from(name_split.nth(1).unwrap());
                if config.import_server(name, cur_path, version, instance_type, instance_ver) {
                    num_servers += 1;
                }
            }
        }
    }

    LOG.line_basic(format!("Imported {num_servers} Minecraft servers"), true);
}