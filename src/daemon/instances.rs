use crate::{
    LOG,
    client::ServerResult,
    mcserver::{
        ServerProfile,
        InstanceType,
        MCVersion,
        TMP_DIR,
    }
};
use std::{
    env, fs::{self, File}, thread,
    io::Write,
    time::Instant,
    process::{Child, Command, Stdio}
};
use crossbeam_channel::bounded;
use rust_utils::{
    utils::run_command,
    logging::LogLevel
};

pub struct Servers { servers: Vec<ServerInstance> }

impl Servers {
    pub fn new() -> Servers {
        Servers {
            servers: vec![]
        }
    }

    pub fn is_running(&self, name: &str) -> bool {
        for server in &self.servers {
            if server.profile.name.as_str() == name {
                return true;
            }
        }
        false
    }

    pub fn get_console(&self, name: &str) -> ServerResult<String> {
        let console_str = match fs::read_to_string(format!("{}/{name}.console", &*TMP_DIR)) {
            Ok(string) => string,
            Err(_) => return Err(format!("Unable to find {name} Minecraft server! Is it running?"))?
        };

        Ok(console_str)
    }

    pub fn add_server(&mut self, instance: ServerInstance) { self.servers.push(instance); }

    pub fn restart_server(&mut self, name: &str) -> ServerResult<()> {
        let instance = match self.get_instance(name) {
            Some(i) => i,
            None => return Err(format!("Unable to find \"{name}\" Minecraft server! Is it running?"))?
        };

        let profile = instance.profile.clone();
        let new_instance = ServerInstance::launch(&profile);
        self.servers.retain(|server| server.profile.name.as_str() != name);
        self.servers.push(new_instance);
        Ok(())
    }

    pub fn shut_down_server(&mut self, name: &str) {
        self.servers.retain(|server| server.profile.name.as_str() != name);
    }

    pub fn shutdown_all(&mut self) { self.servers.clear(); }

    // drops all the handles to the dead server processes
    pub fn cleanup_dead(&mut self) { self.servers.retain(|server| !server.is_dead()); }

    pub fn exec_command(&mut self, name: &str, cmd: &str) -> ServerResult<()> {
        let instance = match self.get_instance_mut(name) {
            Some(i) => i,
            None => return Err(format!("Unable to find \"{name}\" Minecraft server! Is it running?"))?
        };

        let stdin = instance.process.as_mut().unwrap().stdin.as_mut().unwrap();
        writeln!(stdin, "{cmd}").expect("Unable to execute command!");
        Ok(())
    }

    // mutable ref to a server instance
    pub fn get_instance_mut(&mut self, name: &str) -> Option<&mut ServerInstance> {
        self.servers.iter_mut().find(|server| server.profile.name.as_str() == name)
    }

    // immutable ref to a server instance
    pub fn get_instance(&mut self, name: &str) -> Option<&ServerInstance> {
        self.servers.iter().find(|&server| server.profile.name.as_str() == name)
    }
}

pub struct ServerInstance {
    process: Option<Child>,
    profile: ServerProfile,
    pid: usize
}

impl ServerInstance {
    pub fn launch(profile: &ServerProfile) -> ServerInstance {
        LOG.line_basic(format!("Launching \"{}\" Minecraft server...", profile.name), true);
        env::set_current_dir(&profile.directory).expect("where is the server directory?");
        let java_ops = profile.java_ops.clone();
        let version = profile.mc_version;
        let mut server_file =  format!("minecraft_server.{}.jar", profile.mc_version);
        if fs::read(&server_file).is_err() {
            server_file = format!("minecraft-{}-server.jar", profile.mc_version);
        }
        let mut launch = Command::new(&java_ops.launch_command);
        launch.arg(format!("-Xmx{}M", java_ops.max_mem)).arg(format!("-Xms{}M", java_ops.min_mem));

        if version >= MCVersion(1, 17, 0) && version <= MCVersion(1, 18, 0) {
            launch.arg("-Dlog4j2.formatMsgNoLookups=true");
        }
        else if version >= MCVersion(1, 12, 0) && version <= MCVersion(1, 16, 5) {
            launch.arg("-Dlog4j.configurationFile=log4j2_112-116.xml");
        }

        if profile.instance_type == InstanceType::Forge {
            let forge_ver = profile.instance_ver.forge_version().unwrap();
            if version >= MCVersion(1, 17, 0) {
                let forge_arg_path = format!("libraries/net/minecraftforge/forge/{}-{forge_ver}/unix_args.txt", profile.mc_version);
                let forge_arg = format!("@{forge_arg_path}");
                launch.arg(&forge_arg);
            }
            else {
                launch.arg("-jar");
                let forge_jar = format!("forge-{}-{forge_ver}.jar", profile.mc_version);
                launch.arg(&forge_jar);
            }
        }
        else if profile.instance_type == InstanceType::Fabric {
            let (loader_ver, _) = profile.instance_ver.fabric_versions().unwrap();
            let fabric_name = format!("fabric-server-mc.{}-loader.{loader_ver}", profile.mc_version);

            // find the fabric jar
            if let Ok(dir) = fs::read_dir(&profile.directory) {
                for file in dir.filter_map(Result::ok) {
                    let name = file.file_name().into_string().unwrap();

                    // when found add the launch arguments for it
                    if name.starts_with(&fabric_name) {
                        launch.arg("-jar").arg(&name);
                        break;
                    }
                }
            }
        }
        else {
            launch.arg("-jar").arg(&server_file);
        }

        launch.arg("nogui");

        let file = File::create(format!("{}/{}.console", &*TMP_DIR, profile.name)).expect("Unable to create file!");
        let file2 = File::open(format!("{}/{}.console", &*TMP_DIR, profile.name)).expect("Unable to open file!");
        let process = launch.stdout(Stdio::from(file)).stdin(Stdio::piped()).stderr(Stdio::from(file2)).spawn().expect("Failed to launch Minecraft server!");
        let pid = process.id() as usize;

        ServerInstance {
            process: Some(process),
            profile: profile.clone(),
            pid
        }
    }

    // a server is considered dead if it has exited on its own or crashed
    pub fn is_dead(&self) -> bool {
        if let Ok(status) = fs::read_to_string(format!("/proc/{}/status", self.pid)) {
            status.as_str() == "zombie"
        }
        else {
            true
        }
    }
}

impl Drop for ServerInstance {
    fn drop(&mut self) {
        if self.is_dead() {
            LOG.line(LogLevel::Warn, format!("\"{}\" Minecraft server died! Did you or an op execute /stop? If not, check your crash reports folder.", self.profile.name), true);
        }
        else {
            LOG.line_basic(format!("Shutting down \"{}\" Minecraft server!", self.profile.name), true);
            run_command("kill", false, ["-2", &self.pid.to_string()]);
        }

        // stop the server in another thread
        let (snd, rcv) = bounded::<()>(0);
        let mut process = self.process.take().unwrap();
        thread::spawn(move || {
            process.wait().expect("Minecraft server wasn't running!");
            snd.send(()).unwrap_or(()); // notify when it is finished
        });

        let timer = Instant::now();
        let mut kill_proc = true;
        while timer.elapsed().as_secs() < 30 {
            // if the server stops in time, don't kill it
            if rcv.try_recv().is_ok() {
                kill_proc = false;
                break;
            }
        }

        // otherwise, fire in the hole!
        if kill_proc {
            run_command("kill", false, ["-9", &self.pid.to_string()]);
            LOG.line(LogLevel::Warn, format!("\"{}\" Minecraft server has been killed.", self.profile.name), true);
            LOG.line(LogLevel::Warn, "It took more than 30 seconds to shut down", true);
        }
    }
}