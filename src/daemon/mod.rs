use crate::{
    LOG,
    config::{GlobalConfig, Config},
    client::{
        ClientRequest,
        ConsoleLines,
        ServerResult,
        SOCKET
    },
    mcserver::{
        TMP_DIR,
        ServerProfile
    }
};
use std::{
    fs, thread,
    sync::RwLock,
    fmt::Display,
    process,
    io::{BufReader, Write, Read},
    os::unix::net::{UnixStream, UnixListener}
};
use rust_utils::logging::LogLevel;
use lazy_static::lazy_static;
use serde::{Serialize, Deserialize};

mod instances;
mod importer;

use instances::{Servers, ServerInstance};

const SUCCESS: ServerResult<()> = Ok(());

lazy_static! {
    static ref SERVERS: RwLock<Servers> = RwLock::new(Servers::new());
}

#[allow(clippy::manual_flatten)]
pub fn init() {
    LOG.line_basic("Starting up Minecraft Server Manager background process...", true);
    fs::create_dir(&*TMP_DIR).unwrap_or(());
    fs::remove_file(&*SOCKET).unwrap_or(());
    let listener = UnixListener::bind(&*SOCKET).unwrap();

    // if Ctrl-C is pressed
    ctrlc::set_handler(move || {
        let mut servers = SERVERS.write().unwrap();
        LOG.line_basic("Shutting down!", true);
        servers.cleanup_dead();
        servers.shutdown_all();
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    LOG.line_basic("Startup complete!", true);

    // clean up all dead servers in a background thread
    thread::spawn(move || {
        // sometimes Symeon, infinite loops can actually be useful!
        loop {
            SERVERS.write().unwrap().cleanup_dead();
        }
    });

    for request in listener.incoming() {
        if let Ok(stream) = request {
            thread::spawn(move || exec_req(stream));
        }
    }
}

fn exec_req(stream: UnixStream) {
    let mut out_stream = stream.try_clone().unwrap();
    let buffer = BufReader::new(&stream);
    let encoded: Vec<u8> = buffer.bytes().map(|r| r.unwrap_or(0)).collect();
    let request: ClientRequest = bincode::deserialize(&encoded).expect("Error parsing request!");

    match request {
        ClientRequest::Delete(ref name) => {
            if SERVERS.read().unwrap().is_running(name) {
                err_msg(&mut out_stream, "Running servers cannot be deleted!");
                return;
            }

            let mut config = GlobalConfig::load();
            config.remove_profile(name);
            LOG.line_basic(format!("Deleted \"{name}\" Minecraft server"), true);
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::Launch(ref name) => {
            if SERVERS.read().unwrap().is_running(name) {
                err_msg(&mut out_stream, format!("{name} is already running!"));
                return;
            }

            let config = GlobalConfig::load();
            let profile = match config.get_profile(name) {
                Some(p) => p,
                None => {
                    err_msg(&mut out_stream, format!("Server launch profile \"{name}\" does not exist!"));
                    return;
                }
            };

            SERVERS.write().unwrap().add_server(ServerInstance::launch(profile));
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::Restart(ref name) => {
            LOG.line_basic(format!("Restarting \"{name}\" Minecraft server"), true);
            report_result(&mut out_stream, SERVERS.write().unwrap().restart_server(name))
        }

        ClientRequest::Stop(ref name) => {
            SERVERS.write().unwrap().shut_down_server(name);
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::UpdateModloader(ref name) => {
            if SERVERS.read().unwrap().is_running(name) {
                err_msg(&mut out_stream, "Running servers cannot be updated!");
                return;
            }

            let mut config = GlobalConfig::load();
            if let Some(profile) = config.get_profile_mut(name) {
                report_result(&mut out_stream, profile.update_modloader());
                config.save().unwrap();
            }
            else {
                err_msg(&mut out_stream, format!("Server launch profile \"{name}\" does not exist!"));
            }
        }

        ClientRequest::Console(ref name, num_lines) => {
            let console = match SERVERS.read().unwrap().get_console(name) {
                Ok(string) => string,

                Err(error) => {
                    report_result(&mut out_stream, Err(error));
                    return;
                }
            };

            let mut lines = console.lines();
            let cur_numlines = console.lines().count();

            if num_lines > 0 {
                lines.nth(num_lines);
            }

            let data = if cur_numlines == num_lines {
                ConsoleLines::empty()
            }
            else {
                lines.collect::<ConsoleLines>()
            };

            send_val(&mut out_stream, data);
        }

        ClientRequest::Rename(ref name, ref new_name) => {
            if SERVERS.read().unwrap().is_running(name) {
                err_msg(&mut out_stream, "Running servers cannot be renamed!");
                return;
            }
            let mut config = GlobalConfig::load();
            if let Some(server) = config.get_profile_mut(name) {
                server.rename(new_name);
            }
            config.save().unwrap();
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::ImportServers => {
            importer::import_servers();
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::SetupServer {
            ref name,
            ref java,
            mc_version,
            eula,
            instance_type,
            no_chat_reports
        } => {
            let directory = format!("{}/{name}", GlobalConfig::load().default_dir);
            if let Ok(instance_ver) = instance_type.latest_version(mc_version) {
                let profile = ServerProfile::new(name, java, mc_version, &directory, instance_type, instance_ver);
                report_result(&mut out_stream, profile.setup(eula, no_chat_reports));
            }
        }

        ClientRequest::ServerRunning(ref name) => send_val(&mut out_stream, SERVERS.read().unwrap().is_running(name)),
        ClientRequest::ExecCmd(ref name, ref cmd) => report_result(&mut out_stream, SERVERS.write().unwrap().exec_command(name, cmd))
    }
}

fn err_msg<M: Display>(stream: &mut UnixStream, msg: M) {
    report_result(stream, Err(format!("{msg}").into()));
}

fn report_result(stream: &mut UnixStream, result: ServerResult<()>) {
    if let Err(error) = result {
        LOG.line(LogLevel::Error, &error, true);
        send_val(stream, error.to_string());
    }
    else {
        send_val(stream, ());
    }
}

fn send_val<V>(stream: &mut UnixStream, val: V)
    where V: Serialize + for <'de> Deserialize<'de> + ?Sized + 'static
{
    let encoded = bincode::serialize(&val).unwrap();
    if let Err(why) = stream.write_all(&encoded) {
        LOG.line(LogLevel::Warn, format!("Unable to write to socket: {why}"), false);
        LOG.line(LogLevel::Warn, "A console viewer may have crashed or been killed", false);
    };
}