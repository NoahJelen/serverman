# Minecraft Server Manager 
![](icon.png)

Command line program to make creating and managing Minecraft servers easy!

## Disclaimer: I am in no way affiliated with Mojang AB or Microsoft. This program is my own work.

# Screenshots

## Command line interface. This is useful for advanced server setups
![](screenshots/cli.png)

## Text User Interface
![](screenshots/tui.png)

## Server Settings
![](screenshots/settings.png)

## Latest Log of a server that shut down
![](screenshots/server_log.png)

## Minecraft Server Console
![](screenshots/console.png)

## Creating a new server
![](screenshots/new_server.png)

## Crash Report Viewer
![](screenshots/crash_report1.png)
![](screenshots/crash_report2.png)

## FAQ

Can I set up a Minecraft 1.7.10 or 1.12.2 server?

You can set up either of these, but I only support running of versions 1.12 and later (You're blessed I do this, because Forge no longer supports 1.12). Minecraft 1.7.10 will run (including with forge) but older versions of Minecraft may crash or cause wonky behavior.

## Installation

### Arch Linux and others based on it
Use your AUR helper to install `serverman`

## Planned Features:
- [ ] Web interface (ChaosDog might like this)
- [ ] Modpack download links
- [ ] Broadcast to clients whether a server is running or not
- [ ] Update Minecraft to a new version
- [ ] Start a server when a player attempts to connect to it
- [ ] Formatted MOTD in TUI and CLI
- [ ] Use NeoForge instead of Minecraft Forge for Minecraft 1.20+
- [ ] Shell completion for command line interface
- [x] Allow user to view logs of servers that unexpectedly stop working
- [x] Allow user to view main program log